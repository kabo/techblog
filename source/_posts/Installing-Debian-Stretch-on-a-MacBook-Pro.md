---
title: Installing Debian Stretch on a MacBook Pro
date: 2017-02-18 21:50:31
tags:
  - linux
  - debian
  - setup
  - macbook
  - apple
---

When one of my colleagues moved on to a new job I inherited his laptop, a 13" MacBook Pro (macbook11,1). Here's what I did in order to install Debian Stretch on it, step-by-step.

![](/images/appletux.jpg)

First I booted the laptop while holding down `cmd-opt-R` to boot in rescue mode. Then I erased the disk and set up a 50GB partition which I called `Macintosh HD`, and left the rest of the disk as free space. Then I made a clean install of Mac OS X on the newly created partition.

After the installation was complete, I booted Mac OS X and updated it so I had all the latest security patches and so on. I then downloaded the `netinst` image for `amd64` (including non-free firmware, sad) from the [Debian download page](http://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/stretch_di_rc2/amd64/iso-cd/), and ran `sudo dd bs=4M if=/path/to/debian.iso of=/dev/sdb && sync`.

I made sure to plug in the thunderbolt ethernet adapter before inserting the USB and starting the MacBook while holding down `opt`. For some reason, two "EFI boot" USB images appeared. I chose the first one. It booted from the USB and I picked `install`, not `graphical install`. When I came to the disk partitioning screen I chose `manual` option.

- Select the free space, create a 1GB partition at the beginning of the free space. Use as: ext4. Mount point: /boot
- Select the free space, create a partition with the rest of the free space. Use as: physical volume for encryption
- Select 'configure encrypted volumes'
  - create encrypted volumes
  - select your partition
  - finish
- Select encrypted space
  - use as physical volume for LVM
- Configure the logical volume manager
  - create volume group
  - create logical volume, 50GB rootvol
  - create logical volume, 8GB swap
  - create logical volume, max GB homevol
- Select rootvol, use as btrfs, mount point /
- Select swap, use as swap
- Select homevol, use as btrfs, mount point /home
- Finished partitioning and write changes to disk

I installed Debian with the LXDE desktop environment.

Once logged in:

``` bash
$ su -
# usermod -a -G sudo [username]
```

Log out and log back in. Let's take care of that scaling, everything on the screen is tiny.

``` bash
$ sudo apt install synapse htop tmux build-essential shutter cmake clang golang rxvt-unicode-256color curl git python-dev python-pip libssl-dev mlocate chromium vlc irssi baobab deja-dup gufw gparted numix-icon-theme neovim
$ sudo update-alternatives --config editor (select nvim)
$ sudo mkdir -p /etc/X11/xorg.conf.d
$ sudo vim /etc/X11/xorg.conf.d/90-monitor.conf
```

Enter the following:

```
Section "Monitor"
    Identifier             "<default monitor>"
    DisplaySize            286 179    # In millimeters
EndSection
```

``` bash
$ echo "export GDK_SCALE=2" >> /etc/profile
```

Then I right clicked on the bottom panel > panel settings. Here I set the panel height to 52px and the icons to 50px. Then I went to Preferences > Customize Look and Feel. Here I set the icon theme to Numix, widget default font sans size 7 and adjusted the window border font sizes. Then I rebooted.

Let's take care of the wifi. Running `lspci -nn -d 14e4:` returned `[14e4:43a0]`, which is not supported according to https://wireless.wiki.kernel.org/en/users/Drivers/b43 so I need to use wl.

``` bash
$ sudo vim /ets/apt/sources.list # add contrib and non-free, Apple really loves proprietary :(
$ sudo apt install broadcom-sta-dkms
$ sudo iwconfig
```

I think I may have rebooted and/or run some `modprobe` commands here, I can't remember. Sorry.

That last command found interface `wlp3s0`. I clicked wicd > preferences > general and set wireless interface to `wlp3s0`. Now I could connect to my wifi.

To fix the Apple keyboard so it works the way I want it to work (meaning I have to press the `fn`-key to raise the volume and have the `alt`-key next to the space bar):

``` bash
$ echo "1" | sudo tee /sys/module/hid_apple/parameters/swap_opt_cmd # non-permanent, just to try it out
$ echo "2" | sudo tee /sys/module/hid_apple/parameters/fnmode
$ echo options hid_apple fnmode=2 | sudo tee -a /etc/modprobe.d/hid_apple.conf # let's make it permanent
$ echo options hid_apple swap_opt_cmd=1 | sudo tee -a /etc/modprobe.d/hid_apple.conf
$ sudo update-initramfs -u -k all
$ sudo reboot
```

To have neovim behave as I want I needed to download the .deb-files for [python-neovim](https://packages.debian.org/sid/python-neovim) and [python3-neovim](https://packages.debian.org/sid/python3-neovim).

``` bash
$ sudo dpkg -i python-neovim*.deb python3-neovim*.deb
# the above command won't work, but running the command below fixes everything :)
$ sudo apt --fix-broken install
```

I enabled `ufw` using `gufw`, and set `synapse` to run at startup.

There was a bug in `chromium` for Debian ([#852398](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=852398)), which means that one couldn't install addons from the chrome store. Here's how I took care of that.

``` bash
$ echo "CHROMIUM_FLAGS='--enable-remote-extensions'" | sudo tee /etc/environment
```

Now I installed:

- [HTTPS Everywhere](https://chrome.google.com/webstore/detail/https-everywhere/gcbommkclmclpchllfjekcdonpmejbdp)
- [WebRTC Leak Prevent](https://chrome.google.com/webstore/detail/webrtc-leak-prevent/eiadekoaikejlgdbkbdfeijglgfdalml)
- [uBlock Origin](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm)
- [Full Page Screen Capture](https://chrome.google.com/webstore/detail/full-page-screen-capture/fdpohaocaechififmbbbbbknoalclacl)

To install Docker I followed [the instructions](https://docs.docker.com/engine/installation/linux/debian/). Then:

``` bash
$ sudo apt install docker-compose
```

I also installed my dotfiles, you can follow {% post_link First-five-minutes-on-Trisquel my instructions from a previous post %} from the dotfiles part and down, but with the following add on:

``` bash
$ cd ~/.config
$ ln -s ../.vim nvim
$ cd ~/dotfiles/vim
$ ln -s ../../.vimrc init.vim
$ ln -s ~/dotfiles/vim 
$ cd ~
$ vim .bashrc
alias ll='ls -lah'
NPM_PACKAGES="${HOME}/.npm-packages"
PATH="$PATH:$NPM_PACKAGES/bin"
$ vim .npmrc
prefix=${HOME}/.npm-packages
$ mkdir .npm-packages
$ npm install -g eslint
```

Oh, and I installed the Firefox addons described in {% post_link Essential-addons-for-Icecat-Firefox another previous post %}.

Phew! Now I can get to work! Ah, wait, I should probably enable Filevault in Mac OS X as well. After the encryption finishes I reboot to Debian and... Wait, where did my Debian install go? It boots directly in to Mac OS X now?! Arghhh!!!

A few deep breaths later, I boot in to Mac OS X, [download the rEFInd zip-file](http://www.rodsbooks.com/refind/getting.html), unzip it, `cd` into the directory and run `sudo ./refind-install`, reboot and I can once again boot into my Debian. Big scare there for a while...

All good, see you next time!

