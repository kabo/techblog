---
title: Serverless comment system
date: 2016-11-22 20:31:00
tags:
- internet
- serverless
- aws
- comment
- comments
- s3
- react
- lambda
- simpledb
- jwt
---

![](/images/share_love_comment_grafitti.jpeg)

I've just released a little side project, [Serverless lambda comments](https://gitlab.com/kabo/serverless-lambda-comments/). It's a comment system similar to [Disqus](https://disqus.com/), but I'm not comfortable with giving a third party access to the information of the people that comment on my blog. Sure, the information is stored in AWS, but at least it's not in a format that makes it easy for a third party to harvest and sell. If you think that this sounds interesting, this may be a good comment system for you!

I've looked at [lambda-comments](https://github.com/jimpick/lambda-comments) before but I found it too much of a hassle to set up. My intention with this project was to have something that you can clone, edit one config file and deploy. Done!

As this is something that I put together in my spare time it is not something I would recommend for production use just yet, and there is heaps of functionality that is needed to suit all situations.

Released under the [BipCot license](https://bipcot.org/).

{% raw %}
<a href="https://bipcot.org/"><span class="fancybox"><img src="/images/bipicon.jpg" alt="bipicon" style="margin: 0 0 20px 0; display: inline;" /></span></a>
{% endraw %}
