---
title: Setting up a Brother HL-3170CDW in Trisquel Mini
date: 2016-07-06 22:30:45
tags:
- printer
- linux
- trisquel
- brother
- cups
---
My printer is connected to the WiFi. When I tried adding the printer in Trisquel Mini using Trisquel-menu > Preferences > Printers it wouldn't work. The utility found the printer but it always hanged when it tried to install the drivers. Here's what I did to get it to work.

First I made sure my user was in the lpadmin group.
``` bash
$ sudo usermod -aG lpadmin <my-username>
```

I went to the [cups webinterface](http://localhost:631/), clicked "Adding Printers and Classes" and logged in with my account credentials. Then I clicked "Add Printer", selected my printer in the list of discovered printers, selected the make "Brother" and clicked continue. My model, HL-3170CDW wasn't in the list of available drivers though, so I picked the driver `Brother HL-4070CDW BR-Script3`. I then clicked continue. That's all it took to set it up :)
