---
title: Second five minutes on Trisquel
date: 2016-06-25 11:08:41
tags:
---
Here's how I setup some additional stuff, after {% post_link First-five-minutes-on-Trisquel my first five minutes on Trisquel %}.

[ Libreoffice ](https://www.libreoffice.org/):
``` bash
$ sudo add-apt-repository ppa:libreoffice/ppa
$ sudo aptitude update && sudo aptitude -y install libreoffice
```

[ Nightingale ](http://getnightingale.com/):
``` bash
$ sudo add-apt-repository ppa:nightingaleteam/nightingale-release
$ sudo aptitude update && sudo aptitude -y install nightingale
```

[Electrum](https://electrum.org/):
``` bash
$ sudo aptitude install python-qt4
$ sudo pip install https://download.electrum.org/2.6.4/Electrum-2.6.4.tar.gz
```

[OpenBazaar](https://openbazaar.org/download.html) (check out [my store](ob://8a5c58f98f6817d58a447fee69e57a593174c26b/store)):
``` bash
$ sudo dpkg -i openbazaar*.deb
```

[SpiderOak](https://spideroak.com/opendownload):
``` bash
$ sudo dpkg -i spideroakone*.deb
```

Extras:
``` bash
$ sudo aptitude install vlc gnucash gthumb irssi baobab deja-dup gufw
```
