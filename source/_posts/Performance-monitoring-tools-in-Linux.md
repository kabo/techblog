---
title: Performance monitoring tools in Linux
date: 2016-07-02 17:57:33
tags:
- linux
- performance
- infographic
- strace
- lsof
- ltrace
- ss
- pcstat
- pidstat
- perf
- ftrace
- stap
- ktap
- ebpf
- drace
- lttng
- iostat
- iotop
- blktrace
- netstat
- sysdig
- perf
- mpstat
- sar
- dstat
- rdmsr
- top
- ps
- vmstat
- slabtop
- free
- tiptop
- iptraf
- tcpdump
- nicstat
- ip
- lldptool
- snmpget
- ethtool
- swapon
---
[It's FOSS](https://www.facebook.com/itsfoss/) shared this infographic [on Facebook](https://www.facebook.com/itsfoss/photos/a.182637675210341.43761.115098615297581/722830311191072/?type=3). Good to have for reference.

![](/images/performance-tools-linux.jpg)
